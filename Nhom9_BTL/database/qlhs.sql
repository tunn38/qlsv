

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+07:00";



--
-- Database: `qlhs`
--

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `class_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `teach_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`class_id`, `class_name`, `teach_id`) VALUES
(' L1', '10A0', 'GV1'),
(' L2', '10A1', 'GV2'),
(' L3', '10A2', 'GV3');

-- --------------------------------------------------------

--
-- Table structure for table `marks`
--

CREATE TABLE `marks` (
  `st_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sb_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ma_mini_test` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ma_hour_test` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ma_final_exam` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ma_avarage` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `marks`
--

INSERT INTO `marks` (`st_id`, `sb_id`, `ma_mini_test`, `ma_hour_test`, `ma_final_exam`, `ma_avarage`) VALUES
('SV1', 'SB1', '7', '7', '7', '7'),
('SV1', 'SB2', '7', '7', '7', '7'),
('SV1', 'SB3', '7', '7', '7', '7'),
('SV1', 'SB4', '7', '7', '7', '7'),
('SV1', 'SB5', '7', '7', '7', '7');


-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `st_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `st_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `st_birth` date NOT NULL,
  `st_address` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `st_gender` tinyint(1) NOT NULL,
  `st_phone` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `st_email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `st_parent` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class_id` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`st_id`, `st_name`, `st_birth`, `st_address`, `st_gender`, `st_phone`, `st_email`, `st_parent`, `class_id`) VALUES
('SV1', 'Nguyễn Thị A', '2010-11-18', 'Hà Nội', 0, '0123456789', 'a@gmail.com', 'Nguyễn Thị AA', ' L1'),
('SV2', 'Nguyễn Thị  B', '2010-11-05', 'Hà Nội',0, '0123456789', 'b@gmail.com', 'Nguyễn Thị BB', ' L1'),
('SV3', 'Nguyễn Thị C', '2010-01-07', 'Hà Nội', 0, '0123456789', 'c@gmail.com', 'Nguyễn Thị CC', ' L2'),
('SV4', 'Nguyễn Thị D', '2010-11-16', 'Hà Nội', 0, '0123456789', 'd@gmail.com', 'Nguyễn Thị DD', ' L3'),
('SV5', 'Nguyễn Thị E', '2010-01-07', 'Hà Nội', 0, '0123456789', 'e@gmail.com', 'Nguyễn Thị EE', ' L2'),
('SV6', 'Nguyễn Thị F', '2010-11-16', 'Hà Nội', 0, '0123456789', 'f@gmail.com', 'Nguyễn Thị FF', ' L3');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `sb_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sb_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`sb_id`, `sb_name`) VALUES
('SB1', 'Toán học'),
('SB2', 'Hóa học'),
('SB3', 'Ngữ Văn'),
('SB4', 'Sinh Học'),
('SB5', 'Vật Lý');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comment`
--

CREATE TABLE `tbl_comment` (
  `comment_id` int(11) NOT NULL,
  `parent_comment_id` int(11) DEFAULT NULL,
  `comment` varchar(200) NOT NULL,
  `comment_sender_name` varchar(40) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `teach_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `teach_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `teach_email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `teach_phone` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `teach_address` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `teach_gender` tinyint(1) DEFAULT NULL,
  `teach_birth` date DEFAULT NULL,
  `sb_id` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`teach_id`, `teach_name`, `teach_email`, `teach_phone`, `teach_address`, `teach_gender`, `teach_birth`, `sb_id`) VALUES
('GV1', 'Nguyễn Xuân Lộc', 'loc@gmail.com', '0123456789', 'Bắc Ninh', 1, '2001-5-16', 'SB1'),
('GV2', 'Phạm Tuấn Anh', 'anh@gmail.com', '123456789', 'Hà Nam', 1, '2001-12-07', 'SB2'),
('GV3', 'Nguyễn Hoài Linh', 'linh@gmail.com', '123456789', 'Lào Cai', 0, '2001-06-19', 'SB3'),
('GV4', 'Hoàng Kim Lợi', 'loi@gmail.com', '023456789', 'Bắc Ninh', 1, '2005-09-16', 'SB4'),
('GV5', 'Đinh Văn Chính', 'chinh@gmail.com', '0123456789', 'Hà Giang', 1, '2002-01-11', 'SB5');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `account` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `account`, `user_email`, `user_password`, `user_level`) VALUES
('1', 'Nguyễn Xuân Lộc', 'admin', 'loc@gmail.com', 'loc123', 0),
('GV1', 'Nguyễn Xuân Lộc','gvloc', 'loc@gmail.com','gvloc', 1),
('GV2', 'Phạm Tuấn Anh','gvanh', 'anh@gmail.com','gvanh', 1),
('GV3', 'Nguyễn Hoài Linh','gvlinh', 'linh@gmail.com','gvlinh', 1),
('GV4', 'Hoàng Kim Lợi','gvloi', 'loi@gmail.com','gvloi', 1),
('GV5', 'Đinh Văn Chính','gvchinh', 'chinh@gmail.com','gvchinh', 1),
('SV1', 'Nguyễn Thị A', 'a', 'a@gmail.com', 'a', 2),
('SV2', 'Nguyễn Thị B', 'b', 'b@gmail.com', 'b', 2),
('SV3', 'Nguyễn Thị C', 'c', 'c@gmail.com', 'c', 2),
('SV4', 'Nguyễn Thị D', 'd', 'd@gmail.com', 'd', 2),
('SV5', 'Nguyễn Thị E', 'e', 'e@gmail.com', 'e', 2),
('SV6', 'Nguyễn Thị F', 'f', 'f@gmail.com', 'f', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`class_id`),
  ADD KEY `teach_key` (`teach_id`);

--
-- Indexes for table `marks`
--
ALTER TABLE `marks`
  ADD KEY `sb2_key` (`sb_id`),
  ADD KEY `st_key` (`st_id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`st_id`),
  ADD KEY `L_key` (`class_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`sb_id`);

--
-- Indexes for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`teach_id`),
  ADD KEY `sb_key` (`sb_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `classes`
--
ALTER TABLE `classes`
  ADD CONSTRAINT `teach_key` FOREIGN KEY (`teach_id`) REFERENCES `teachers` (`teach_id`);

--
-- Constraints for table `marks`
--
ALTER TABLE `marks`
  ADD CONSTRAINT `sb2_key` FOREIGN KEY (`sb_id`) REFERENCES `subjects` (`sb_id`),
  ADD CONSTRAINT `st_key` FOREIGN KEY (`st_id`) REFERENCES `students` (`st_id`);

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `L_key` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`);

--
-- Constraints for table `teachers`
--
ALTER TABLE `teachers`
  ADD CONSTRAINT `sb_key` FOREIGN KEY (`sb_id`) REFERENCES `subjects` (`sb_id`);
COMMIT;


